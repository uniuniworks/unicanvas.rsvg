﻿using System;
using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	public class Rsvg {
		private IntPtr _handle;

		public string BaseUri {
			get {
				if (_handle == IntPtr.Zero) {
					return string.Empty;
				}
				return NativeMethods.rsvg_handle_get_base_uri(
					_handle
				);
			}
			set {
				if (_handle == IntPtr.Zero) {
					return;
				}
				NativeMethods.rsvg_handle_set_base_uri(
					_handle, value
				);
			}
		}

		//[Obsolete("Since version 2.46")]
		public RsvgDimensionData Dimensions {
			get {
				if (_handle == IntPtr.Zero) {
					return null;
				}
				RsvgDimensionData data = new RsvgDimensionData();
				NativeMethods.rsvg_handle_get_dimensions(
					_handle, data
				);
				return data;
			}
		}

		//[Obsolete("Since version 2.36")]
		public string Title {
			get {
				if (_handle == IntPtr.Zero) {
					return string.Empty;
				}
				return NativeMethods.rsvg_handle_get_title(
					_handle
				);
			}
		}

		//[Obsolete("Since version 2.36")]
		public string Desc {
			get {
				if (_handle == IntPtr.Zero) {
					return string.Empty;
				}
				return NativeMethods.rsvg_handle_get_desc(
					_handle
				);
			}
		}

		//[Obsolete("Since version 2.36")]
		public string Metadata {
			get {
				if (_handle == IntPtr.Zero) {
					return string.Empty;
				}
				return NativeMethods.rsvg_handle_get_metadata(
					_handle
				);
			}
		}

		public static uint ErrorQuark() {
			return NativeMethods.rsvg_error_quark();
		}

		//[Obsolete("Since version 2.46")]
		public static void Cleanup() {
			NativeMethods.rsvg_cleanup();
		}

		//[Obsolete("Since version 2.36")]
		public static void Init() {
			NativeMethods.rsvg_init();
		}

		//[Obsolete("Since version 2.36")]
		public static void Term() {
			NativeMethods.rsvg_term();
		}

		//[Obsolete("Since version 2.42.3")]
		public static void SetDefaultDpi(double dpi) {
			NativeMethods.rsvg_set_default_dpi(dpi);
		}

		//[Obsolete("Since version 2.42.3")]
		public static void SetDefaultDpi(double dpiX, double dpiY) {
			NativeMethods.rsvg_set_default_dpi_x_y(dpiX, dpiY);
		}

		public static IntPtr ErrorGetType() {
			return NativeMethods.rsvg_error_get_type();
		}

		public Rsvg() {
			_handle = NativeMethods.rsvg_handle_new();
			if (_handle == IntPtr.Zero) {
				throw new InvalidOperationException();
			}
		}

		public Rsvg(RsvgHandleFlags flags) {
			_handle = NativeMethods.rsvg_handle_new_with_flags(
				flags
			);
			if (_handle == IntPtr.Zero) {
				throw new InvalidOperationException();
			}
		}

		public Rsvg(string fileName) {
			IntPtr error;
			_handle = NativeMethods.rsvg_handle_new_from_file(
				fileName, out error
			);
			if (_handle == IntPtr.Zero || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		public Rsvg(byte[] data) {
			IntPtr error;
			_handle = NativeMethods.rsvg_handle_new_from_data(
				data, (uint)data.Length, out error
			);
			if (_handle == IntPtr.Zero || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		~Rsvg() {
			Dispose();
		}

		public virtual void Dispose() {
			if (_handle != IntPtr.Zero) {
				NativeMethods.rsvg_handle_free(_handle);
				_handle = IntPtr.Zero;
			}
		}

		public void SetDpi(double dpi) {
			if (_handle == IntPtr.Zero) {
				return;
			}
			NativeMethods.rsvg_handle_set_dpi(
				_handle, dpi
			);
		}

		public void SetDpi(double dpiX, double dpiY) {
			if (_handle == IntPtr.Zero) {
				return;
			}
			NativeMethods.rsvg_handle_set_dpi_x_y(
				_handle, dpiX, dpiY
			);
		}

		//[Obsolete("Since version 2.46")]
		public void Write(byte[] buf) {
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_write(
				_handle, buf, (ulong)buf.Length, out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		//[Obsolete("Since version 2.46")]
		public RsvgDimensionData GetDimensionsSub(string id) {
			if (_handle == IntPtr.Zero) {
				return null;
			}
			RsvgDimensionData data = new RsvgDimensionData();
			bool result = NativeMethods.rsvg_handle_get_dimensions_sub(
				_handle, data, id
			);
			if (result == false) {
				return null;
			}
			return data;
		}

		//[Obsolete("Since version 2.46")]
		public RsvgPositionData GetPositionSub(string id) {
			if (_handle == IntPtr.Zero) {
				return null;
			}
			RsvgPositionData data = new RsvgPositionData();
			bool result = NativeMethods.rsvg_handle_get_position_sub(
				_handle, data, id
			);
			if (result == false) {
				return null;
			}
			return data;
		}

		public bool HasSub(string id) {
			if (_handle == IntPtr.Zero) {
				return false;
			}
			return NativeMethods.rsvg_handle_has_sub(
				_handle, id
			);
		}

		public RsvgDimensions GetIntrinsicDimensions() {
			if (_handle == IntPtr.Zero) {
				return null;
			}
			bool hasWidth, hasHeight, hasViewbox;
			RsvgDimensions dimensions = new RsvgDimensions();
			NativeMethods.rsvg_handle_get_intrinsic_dimensions(
				_handle,
				out hasWidth, dimensions.width,
				out hasHeight, dimensions.height,
				out hasViewbox, dimensions.viewbox
			);
			if (hasWidth == false) {
				dimensions.width = null;
			}
			if (hasHeight == false) {
				dimensions.height = null;
			}
			if (hasViewbox == false) {
				dimensions.viewbox = null;
			}
			return dimensions;
		}

		public void RenderDocument(
			IntPtr cairoContext, RsvgRectangle viewport) {
			if (_handle == IntPtr.Zero || cairoContext == IntPtr.Zero) {
				return;
			}
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_render_document(
				_handle, cairoContext, viewport, out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		public RsvgGeometry GetGeometryForLayer(
			string id, RsvgRectangle viewport)
		{
			if (_handle == IntPtr.Zero) {
				return null;
			}
			RsvgGeometry geometry = new RsvgGeometry();
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_get_geometry_for_layer(
				_handle, id, viewport,
				geometry.inkRect, geometry.logicalRect,
				out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
			return geometry;
		}

		public void RenderLayer(
			IntPtr cairoContext, string id, RsvgRectangle viewport)
		{
			if (_handle == IntPtr.Zero || cairoContext == IntPtr.Zero) {
				return;
			}
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_render_layer(
				_handle, cairoContext, id, viewport, out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		public RsvgGeometry GetGeometryForElement(string id) {
			if (_handle == IntPtr.Zero) {
				return null;
			}
			RsvgGeometry geometry = new RsvgGeometry();
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_get_geometry_for_element(
				_handle, id, geometry.inkRect, geometry.logicalRect, out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
			return geometry;
		}

		public void RenderElement(
			IntPtr cairoContext, string id, RsvgRectangle elementViewport)
		{
			if (_handle == IntPtr.Zero) {
				return;
			}
			IntPtr error;
			bool result = NativeMethods.rsvg_handle_render_element(
				_handle, cairoContext, id, elementViewport, out error
			);
			if (result == false || error != IntPtr.Zero) {
				GError gerror = ToGError(error);
				if (gerror != null) {
					throw new InvalidOperationException(gerror.message);
				}
				throw new InvalidOperationException();
			}
		}

		public bool RenderCairo(IntPtr cairoContext) {
			if (_handle == IntPtr.Zero || cairoContext == IntPtr.Zero) {
				return false;
			}
			return NativeMethods.rsvg_handle_render_cairo(
				_handle, cairoContext
			);
		}

		public bool RenderCairoSub(IntPtr cairoContext, string id) {
			if (_handle == IntPtr.Zero || cairoContext == IntPtr.Zero) {
				return false;
			}
			return NativeMethods.rsvg_handle_render_cairo_sub(
				_handle, cairoContext, id
			);
		}

		private GError ToGError(IntPtr error) {
			if (error == IntPtr.Zero) {
				return null;
			}
			return Marshal.PtrToStructure(
				error, typeof(GError)
			) as GError;
		}
	}
}
