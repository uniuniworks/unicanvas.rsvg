﻿namespace UniCanvas.Rsvg {
	/// <summary>
	/// Units for the RsvgLength struct.
	/// These have the same meaning as CSS length units.
	/// </summary>
	public enum RsvgUnit {
		/// <summary>
		/// percentage values; where 1.0 means 100%.
		/// </summary>
		Percent,

		/// <summary>
		/// pixels
		/// </summary>
		PX,

		/// <summary>
		/// em, or the current font size
		/// </summary>
		EM,

		/// <summary>
		/// x-height of the current font
		/// </summary>
		EX,

		/// <summary>
		/// inches
		/// </summary>
		IN,

		/// <summary>
		/// centimeters
		/// </summary>
		CM,

		/// <summary>
		/// millimeters
		/// </summary>
		MM,

		/// <summary>
		/// points, or 1/72 inch
		/// </summary>
		PT,

		/// <summary>
		/// picas, or 1/6 inch (12 points)
		/// </summary>
		PC
	}
}
