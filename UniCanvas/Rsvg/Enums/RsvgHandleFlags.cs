﻿using System;

namespace UniCanvas.Rsvg {
	[Flags]
	public enum RsvgHandleFlags {
		None = 0,
		Unlimited = 1 << 0,
		KeepImageData = 1 << 1
	}
}
