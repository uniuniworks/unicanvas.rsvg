﻿using System;
using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	using RsvgHandle = IntPtr;
	using cairo_t = IntPtr;

	public class NativeMethods {
		#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		public const string DllPath = "librsvg-2.2";
		#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
		public const string DllPath = "librsvg";
		#else
		public const string DllPath = "librsvg-2.so.2";
		#endif

		public const CallingConvention CallingConv = CallingConvention.Cdecl;

		// --------------------------------------------------------------------
		// RsvgHandle

		/// <summary>
		/// The error domain for RSVG
		/// </summary>
		/// <returns>The error domain</returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static uint rsvg_error_quark();

		/// <summary>
		/// [Since: 2.36]
		/// rsvg_cleanup has been deprecated since version 2.46
		/// and should not be used in newly-written code.
		/// No-op. This function should not be called from normal programs.
		/// </summary>
		//[Obsolete("Since version 2.46")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_cleanup();

		/// <summary>
		/// [Since: 2.8]
		/// Do not use this function.
		/// Create an RsvgHandle and call rsvg_handle_set_dpi() on it instead.
		/// </summary>
		/// <param name="dpi">Dots Per Inch (aka Pixels Per Inch)</param>
		//[Obsolete("Since version 2.42.3")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_set_default_dpi(
			double dpi
		);

		/// <summary>
		/// [Since: 2.8]
		/// Do not use this function.
		/// Create an RsvgHandle and call rsvg_handle_set_dpi_x_y()
		/// on it instead.
		/// </summary>
		/// <param name="dpi_x">Dots Per Inch (aka Pixels Per Inch)</param>
		/// <param name="dpi_y">Dots Per Inch (aka Pixels Per Inch)</param>
		//[Obsolete("Since version 2.42.3")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_set_default_dpi_x_y(
			double dpi_x,
			double dpi_y
		);

		/// <summary>
		/// [Since: 2.8]
		/// Sets the DPI at which the handle will be rendered.
		/// Common values are 75, 90, and 300 DPI.
		/// </summary>
		/// <param name="handle">An RsvgHandle</param>
		/// <param name="dpi">Dots Per Inch (i.e. as Pixels Per Inch)</param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_set_dpi(
			RsvgHandle handle, // RsvgHandle*
			double dpi
		);

		/// <summary>
		/// [Since: 2.8]
		/// Sets the DPI at which the handle will be rendered.
		/// Common values are 75, 90, and 300 DPI.
		/// </summary>
		/// <param name="handle">An RsvgHandle</param>
		/// <param name="dpi_x">Dots Per Inch (i.e. Pixels Per Inch)</param>
		/// <param name="dpi_y">Dots Per Inch (i.e. Pixels Per Inch)</param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_set_dpi_x_y(
			RsvgHandle handle, // RsvgHandle*
			double dpi_x,
			double dpi_y
		);

		/// <summary>
		/// Returns a new rsvg handle.
		/// Must be freed with g_object_unref.
		/// This handle can be used to load an image.
		/// </summary>
		/// <returns>A new RsvgHandle with no flags set.</returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static RsvgHandle rsvg_handle_new();

		/// <summary>
		/// [Since: 2.36]
		/// Creates a new RsvgHandle with flags "flags".
		/// After calling this function, you can feed the resulting handle
		/// with SVG data by using rsvg_handle_read_stream_sync().
		/// </summary>
		/// <param name="flags">flags from RsvgHandleFlags</param>
		/// <returns>a new RsvgHandle</returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static RsvgHandle rsvg_handle_new_with_flags(
			RsvgHandleFlags flags
		);

		/// <summary>
		/// Loads the next count bytes of the image.
		/// </summary>
		/// <param name="handle">an RsvgHandle</param>
		/// <param name="buf">pointer to svg data.</param>
		/// <param name="count">length of the buf buffer in bytes</param>
		/// <param name="error">a location to store a GError, or NULL.</param>
		/// <returns>TRUE on success, or FALSE on error.</returns>
		//[Obsolete("Since version 2.46")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_write(
			RsvgHandle handle, // RsvgHandle*
			byte[] buf,        // const guchar*
			ulong count,       // gsize
			out IntPtr error   // GError**
		);

		/// <summary>
		/// Closes handle, to indicate that loading the image is complete.
		/// This will return TRUE if the loader closed successfully
		/// and the SVG data was parsed correctly.
		/// Note that handle isn't freed until g_object_unref is called.
		/// </summary>
		/// <param name="handle">a RsvgHandle</param>
		/// <param name="error">a location to store a GError, or NULL.</param>
		/// <returns>TRUE on success, or FALSE on error.</returns>
		//[Obsolete("Since version 2.46")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_close(
			RsvgHandle handle, // RsvgHandle*
			out IntPtr error   // GError**
		);

		[DllImport(DllPath, CallingConvention = CallingConv,
			EntryPoint = "rsvg_handle_get_base_uri")]
		private extern static IntPtr _rsvg_handle_get_base_uri(
			RsvgHandle handle // RsvgHandle*
		);
		/// <summary>
		/// [Since: 2.8]
		/// Gets the base uri for this RsvgHandle.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <returns>the base uri, possibly null</returns>
		public static string rsvg_handle_get_base_uri(RsvgHandle handle) {
			IntPtr ptr = _rsvg_handle_get_base_uri(handle);
			return Marshal.PtrToStringAnsi(ptr);
		}

		/// <summary>
		/// [Since: 2.9]
		/// Set the base URI for this SVG.
		/// Note: This function may only be called before rsvg_handle_write()
		/// or rsvg_handle_read_stream_sync() have been called.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="base_uri">The base uri</param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_set_base_uri(
			RsvgHandle handle, // RsvgHandle*
			string base_uri    // const char*
		);

		/// <summary>
		/// [Since: 2.14]
		/// Get the SVG's size.
		/// Do not call from within the size_func callback,
		/// because an infinite loop will occur.
		/// This function depends on the RsvgHandle's DPI to compute dimensions
		/// in pixels, so you should call rsvg_handle_set_dpi() beforehand.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="dimension_data">
		/// A place to store the SVG's size.</param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_get_dimensions(
			RsvgHandle handle,               // RsvgHandle*
			RsvgDimensionData dimension_data // RsvgDimensionData*
		);

		/// <summary>
		/// [Since: 2.22]
		/// Get the size of a subelement of the SVG file.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="dimension_data">
		/// A place to store the SVG's size.</param>
		/// <param name="id">
		/// An element's id within the SVG, starting with "##"
		/// (a single hash character), for example, "#layer1".
		/// This notation corresponds to a URL's fragment ID.
		/// Alternatively, pass NULL to use the whole SVG.
		/// </param>
		/// <returns></returns>
		//[Obsolete("Since version 2.46")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_get_dimensions_sub(
			RsvgHandle handle,                // RsvgHandle*
			RsvgDimensionData dimension_data, // RsvgDimensionData*
			string id                         // const char*
		);

		/// <summary>
		/// [Since: 2.22]
		/// Get the position of a subelement of the SVG file.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="position_data">
		/// A place to store the SVG fragment's position.</param>
		/// <param name="id">
		/// An element's id within the SVG, starting with "##"
		/// (a single hash character), for example, "#layer1".
		/// This notation corresponds to a URL's fragment ID.
		/// Alternatively, pass NULL to use the whole SVG.
		/// </param>
		/// <returns></returns>
		//[Obsolete("Since version 2.46")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_get_position_sub(
			RsvgHandle handle,              // RsvgHandle*
			RsvgPositionData position_data, // RsvgPositionData*
			string id                       // const char*
		);

		/// <summary>
		/// [Since: 2.22]
		/// Checks whether the element id exists in the SVG document.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="id">
		/// An element's id within the SVG, starting with "##"
		/// (a single hash character), for example, "#layer1".
		/// This notation corresponds to a URL's fragment ID.
		/// </param>
		/// <returns>
		/// TRUE if id exists in the SVG document, FALSE otherwise.
		/// </returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_has_sub(
			RsvgHandle handle, // RsvgHandle*
			string id          // const char*
		);

		[DllImport(DllPath, CallingConvention = CallingConv,
			EntryPoint = "rsvg_handle_get_title")]
		private extern static IntPtr _rsvg_handle_get_title(
			RsvgHandle handle // RsvgHandle*
		);
		/// <summary>
		/// [Since: 2.4]
		/// rsvg_handle_get_title has been deprecated since version 2.36.
		/// and should not be used in newly-written code.
		/// Librsvg does not read the metadata/desc/title elements;
		/// this function always returns NULL.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <returns>This function always returns NULL.</returns>
		//[Obsolete("Since version 2.36")]
		public static string rsvg_handle_get_title(RsvgHandle handle) {
			IntPtr ptr = _rsvg_handle_get_title(handle);
			return Marshal.PtrToStringAnsi(ptr);
		}

		[DllImport(DllPath, CallingConvention = CallingConv,
			EntryPoint = "rsvg_handle_get_desc")]
		private extern static IntPtr _rsvg_handle_get_desc(
			RsvgHandle handle // RsvgHandle*
		);
		/// <summary>
		/// [Since: 2.4]
		/// rsvg_handle_get_desc has been deprecated since version 2.36.
		/// and should not be used in newly-written code.
		/// Librsvg does not read the metadata/desc/title elements;
		/// this function always returns NULL.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <returns>This function always returns NULL.</returns>
		//[Obsolete("Since version 2.36")]
		public static string rsvg_handle_get_desc(RsvgHandle handle) {
			IntPtr ptr = _rsvg_handle_get_desc(handle);
			return Marshal.PtrToStringAnsi(ptr);
		}

		[DllImport(DllPath, CallingConvention = CallingConv,
			EntryPoint = "rsvg_handle_get_metadata")]
		private extern static IntPtr _rsvg_handle_get_metadata(
			RsvgHandle handle // RsvgHandle*
		);
		/// <summary>
		/// [Since: 2.9]
		/// rsvg_handle_get_metadata has been deprecated since version 2.36.
		/// and should not be used in newly-written code.
		/// Librsvg does not read the metadata/desc/title elements;
		/// this function always returns NULL.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <returns>This function always returns NULL.</returns>
		//[Obsolete("Since version 2.36")]
		public static string rsvg_handle_get_metadata(RsvgHandle handle) {
			IntPtr ptr = _rsvg_handle_get_metadata(handle);
			return Marshal.PtrToStringAnsi(ptr);
		}

		/// <summary>
		/// [Since: 2.14]
		/// Loads the SVG specified by data.
		/// Note that this function creates an RsvgHandle without a base URL,
		/// and without any RsvgHandleFlags.
		/// If you need these, use rsvg_handle_new_from_stream_sync()
		/// instead by creating a GMemoryInputStream from your data.
		/// </summary>
		/// <param name="data">The SVG data.</param>
		/// <param name="data_len">The length of data, in bytes</param>
		/// <param name="error">return location for errors.</param>
		/// <returns>A RsvgHandle or NULL if an error occurs.</returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr rsvg_handle_new_from_data( // RsvgHandle
			byte[] data,     // const guint8*
			uint data_len,   // gsize
			out IntPtr error // GError**
		);

		/// <summary>
		/// [Since: 2.14]
		/// Loads the SVG specified by file_name.
		/// Note that this function, like rsvg_handle_new(),
		/// does not specify any loading flags for the resulting handle.
		/// If you require the use of RsvgHandleFlags,
		/// use rsvg_handle_new_from_gfile_sync().
		/// </summary>
		/// <param name="file_name">The file name to load, or a URI.</param>
		/// <param name="error">return location for errors.</param>
		/// <returns>A RsvgHandle or NULL if an error occurs.</returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static RsvgHandle rsvg_handle_new_from_file(
			string file_name, // const gchar*
			out IntPtr error  // GError**
		);

		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr rsvg_error_get_type(); // GType

		/// <summary>
		/// [Since: 2.9]
		/// This function does nothing.
		/// rsvg_init has been deprecated since version 2.36
		/// and should not be used in newly-written code.
		/// There is no need to initialize librsvg.
		/// </summary>
		//[Obsolete("Since version 2.36")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_init();

		/// <summary>
		/// [Since: 2.9]
		/// This function does nothing.
		/// rsvg_term has been deprecated since version 2.36
		/// and should not be used in newly-written code.
		/// There is no need to de-initialize librsvg.
		/// </summary>
		//[Obsolete("Since version 2.36")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_term();

		/// <summary>
		/// Frees handle.
		/// rsvg_handle_free is deprecated and should not be
		/// used in newly-written code.
		/// Use g_object_unref() instead.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		//[Obsolete("Use g_object_unref() instead")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_free(
			RsvgHandle handle // RsvgHandle*
		);

		/// <summary>
		/// Sets the sizing function for the handle,
		/// which can be used to override the size that
		/// librsvg computes for SVG images.
		/// rsvg_handle_set_size_callback has been deprecated
		/// since version 2.14.
		/// </summary>
		/// <param name="handle">A RsvgHandle</param>
		/// <param name="size_func">A sizing function, or NULL.</param>
		/// <param name="user_data">
		/// User data to pass to size_func, or NULL</param>
		/// <param name="user_data_destroy">
		/// Function to be called to destroy the data passed
		/// in user_data, or NULL.</param>
		//[Obsolete("Since version 2.14")]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_set_size_callback(
			RsvgHandle handle,      // RsvgHandle*
			RsvgSizeFunc size_func, // RsvgSizeFunc
			byte[] user_data,       // gpointer
			GDestroyNotify user_data_destroy // GDestroyNotify
		);

		/*
		[DllImport(DllPath, CallingConvention = CallingConv,
			EntryPoint = "librsvg_version")]
		private extern static IntPtr _librsvg_version();
		public static string librsvg_version() {
			IntPtr ptr = _librsvg_version();
			return Marshal.PtrToStringAnsi(ptr);
		}
		*/

		// --------------------------------------------------------------------
		// Using RSVG with GIO

		/// <summary>
		/// Set the base URI for handle from file.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="base_file"></param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_set_base_gfile(
			RsvgHandle handle, // RsvgHandle*
			IntPtr base_file   // GFile*
		);

		/// <summary>
		/// Reads stream and writes the data from it to handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="stream"></param>
		/// <param name="cancellable"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_read_stream_sync(
			RsvgHandle handle,  // RsvgHandle*
			IntPtr stream,      // GInputStream*
			IntPtr cancellable, // GCancellable*
			out IntPtr error    // GError**
		);

		/// <summary>
		/// Creates a new RsvgHandle for file.
		/// </summary>
		/// <param name="file"></param>
		/// <param name="flags"></param>
		/// <param name="cancellable"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static RsvgHandle rsvg_handle_new_from_gfile_sync(
			IntPtr file,           // GFile*
			RsvgHandleFlags flags,
			IntPtr cancellable,    // GCancellable*
			out IntPtr error       // GError**
		);

		/// <summary>
		/// Creates a new RsvgHandle for stream.
		/// </summary>
		/// <param name="input_stream"></param>
		/// <param name="base_file"></param>
		/// <param name="flags"></param>
		/// <param name="cancellable"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static RsvgHandle rsvg_handle_new_from_stream_sync(
			IntPtr input_stream,   // GInputStream*
			IntPtr base_file,      // GFile*
			RsvgHandleFlags flags,
			IntPtr cancellable,    // GCancellable*
			out IntPtr error       // GError**
		);

		// --------------------------------------------------------------------
		// Using RSVG with cairo

		/// <summary>
		/// Queries the width, height, and viewBox attributes in
		/// an SVG document.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="out_has_width"></param>
		/// <param name="out_width"></param>
		/// <param name="out_has_height"></param>
		/// <param name="out_height"></param>
		/// <param name="out_has_viewbox"></param>
		/// <param name="out_viewbox"></param>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static void rsvg_handle_get_intrinsic_dimensions(
			RsvgHandle handle,        // RsvgHandle*
			out bool out_has_width,   // gboolean*
			RsvgLength out_width,     // RsvgLength*
			out bool out_has_height,  // gboolean*
			RsvgLength out_height,    // RsvgLength*
			out bool out_has_viewbox, // gboolean*
			RsvgRectangle out_viewbox // RsvgRectangle*
		);

		/// <summary>
		/// Renders the whole SVG document fitted to a viewport.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="cr"></param>
		/// <param name="viewport"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_render_document(
			RsvgHandle handle,      // RsvgHandle*
			cairo_t cr,             // cairo_t*
			RsvgRectangle viewport, // const RsvgRectangle*
			out IntPtr error        // GError**
		);

		/// <summary>
		/// Computes the ink rectangle and logical rectangle of an SVG element,
		/// or the whole SVG, as if the whole SVG were rendered
		/// to a specific viewport.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="id"></param>
		/// <param name="viewport"></param>
		/// <param name="out_ink_rect"></param>
		/// <param name="out_logical_rect"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_get_geometry_for_layer(
			RsvgHandle handle,              // RsvgHandle*
			string id,                      // const char*
			RsvgRectangle viewport,         // const RsvgRectangle*
			RsvgRectangle out_ink_rect,     // RsvgRectangle*
            RsvgRectangle out_logical_rect, // RsvgRectangle*
			out IntPtr error                // GError**
		);

		/// <summary>
		/// Renders a single SVG element in the same place
		/// as for a whole SVG document.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="cr"></param>
		/// <param name="id"></param>
		/// <param name="viewport"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_render_layer(
			RsvgHandle handle,      // RsvgHandle*
			cairo_t cr,             // cairo_t*
			string id,              // const char*
			RsvgRectangle viewport, // const RsvgRectangle*
			out IntPtr error        // GError**
		);

		/// <summary>
		/// Computes the ink rectangle and logical rectangle
		/// of a singe SVG element.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="id"></param>
		/// <param name="out_ink_rect"></param>
		/// <param name="out_logical_rect"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_get_geometry_for_element(
			RsvgHandle handle, // RsvgHandle*
			string id, // const char*
			RsvgRectangle out_ink_rect, // RsvgRectangle*
			RsvgRectangle out_logical_rect, // RsvgRectangle*
			out IntPtr error // GError**
		);

		/// <summary>
		/// Renders a single SVG element to a given viewport
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="cr"></param>
		/// <param name="id"></param>
		/// <param name="element_viewport"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_render_element(
			RsvgHandle handle,              // RsvgHandle*
			cairo_t cr,                     // cairo_t*
			string id,                      // const char*
			RsvgRectangle element_viewport, // const RsvgRectangle*
			out IntPtr error                // GError**
		);

		/// <summary>
		/// Draws a loaded SVG handle to a Cairo context.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="cr"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_render_cairo(
			RsvgHandle handle, // RsvgHandle*
			cairo_t cr         // cairo_t*
		);

		/// <summary>
		/// Draws a subset of a loaded SVG handle to a Cairo context.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="cr"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool rsvg_handle_render_cairo_sub(
			RsvgHandle handle, // RsvgHandle*
			cairo_t cr,        // cairo_t*
			string id          // const char*
		);

		// --------------------------------------------------------------------
		// Using RSVG with GdkPixbuf

		/// <summary>
		/// Returns the pixbuf loaded by handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_handle_get_pixbuf(
			RsvgHandle handle // RsvgHandle*
		);

		/// <summary>
		/// Creates a GdkPixbuf the same size as the entire SVG loaded
		/// into handle, but only renders the sub-element that has
		/// the specified id (and all its sub-sub-elements recursively).
		/// If id is NULL, this function renders the whole SVG.
		/// </summary>
		/// <param name="handle"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_handle_get_pixbuf_sub(
			RsvgHandle handle, // RsvgHandle*
			string id          // const char*
		);

		/// <summary>
		/// Loads a new GdkPixbuf from filename and returns it.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		//[Obsolete]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_pixbuf_from_file(
			string filename, // const gchar*
			out IntPtr error // GError**
		);

		/// <summary>
		/// Loads a new GdkPixbuf from filename and returns it.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="x_zoom"></param>
		/// <param name="y_zoom"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		//[Obsolete]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_pixbuf_from_file_at_zoom(
			string filename, // const gchar*
			double x_zoom,
			double y_zoom,
			out IntPtr error // GError**
		);

		/// <summary>
		/// Loads a new GdkPixbuf from filename and returns it.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		//[Obsolete]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_pixbuf_from_file_at_size(
			string filename, // const gchar*
			int width,       // gint
            int height,      // gint
			out IntPtr error // GError**
		);

		/// <summary>
		/// Loads a new GdkPixbuf from filename and returns it.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="max_width"></param>
		/// <param name="max_height"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		//[Obsolete]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_pixbuf_from_file_at_max_size(
			string filename, // const gchar*
			int max_width,   // gint
            int max_height,  // gint
			out IntPtr error // GError**
		);

		/// <summary>
		/// Loads a new GdkPixbuf from filename and returns it.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="x_zoom"></param>
		/// <param name="y_zoom"></param>
		/// <param name="max_width"></param>
		/// <param name="max_height"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		//[Obsolete]
		[DllImport(DllPath, CallingConvention = CallingConv)]
		public extern static IntPtr // GdkPixbuf*
		rsvg_pixbuf_from_file_at_zoom_with_max(
			string filename, // const gchar*
			double x_zoom,
			double y_zoom,
			int max_width,   // gint
            int max_height,  // gint
			out IntPtr error // GError**
		);
	}
}
