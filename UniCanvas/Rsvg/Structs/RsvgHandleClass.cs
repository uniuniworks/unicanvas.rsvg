﻿using System;
using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	/// <summary>
	/// Class structure for RsvgHandle.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public class RsvgHandleClass {
		/// <summary>
		/// parent class
		/// </summary>
		public IntPtr parent; // GObjectClass
	}
}
