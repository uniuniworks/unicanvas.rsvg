﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	/// <summary>
	/// Position of an SVG fragment from rsvg_handle_get_position_sub().
	/// Please the deprecation documentation for that function.
	/// </summary>
	//[Obsolete("Since version 2.46")]
	[StructLayout(LayoutKind.Sequential)]
	public class RsvgPositionData {
		/// <summary>
		/// position on the x axis
		/// </summary>
		public int x;

		/// <summary>
		/// position on the y axis
		/// </summary>
		public int y;
	}
}
