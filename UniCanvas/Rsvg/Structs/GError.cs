﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	[StructLayout(LayoutKind.Sequential)]
	public class GError {
		public uint domain;    // GQuark
		public int code;       // gint
		public string message; // gchar*
	}
}
