﻿namespace UniCanvas.Rsvg {
	public class RsvgGeometry {
		public RsvgRectangle inkRect;
		public RsvgRectangle logicalRect;

		public RsvgGeometry() {
			inkRect = new RsvgRectangle();
			logicalRect = new RsvgRectangle();
		}
	}
}
