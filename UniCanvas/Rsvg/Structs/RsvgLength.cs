﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	/// <summary>
	/// RsvgLength values are used in rsvg_handle_get_intrinsic_dimensions(),
	/// for example, to return the CSS length values of the width and height
	/// attributes of an &lt;svg&gt; element.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public class RsvgLength {
		/// <summary>
		/// numeric part of the length
		/// </summary>
		public double length;

		/// <summary>
		/// unit part of the length
		/// </summary>
		public RsvgUnit unit;
	}
}
