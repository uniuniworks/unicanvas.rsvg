﻿namespace UniCanvas.Rsvg {
	public class RsvgDimensions {
		public RsvgLength width;
		public RsvgLength height;
		public RsvgRectangle viewbox;

		public RsvgDimensions() {
			width = new RsvgLength();
			height = new RsvgLength();
			viewbox = new RsvgRectangle();
		}
	}
}
