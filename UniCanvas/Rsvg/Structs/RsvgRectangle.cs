﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	/// <summary>
	/// A data structure for holding a rectangle.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public class RsvgRectangle {
		/// <summary>
		/// X coordinate of the left side of the rectangle
		/// </summary>
		public double x;

		/// <summary>
		/// Y coordinate of the the top side of the rectangle
		/// </summary>
		public double y;

		/// <summary>
		/// width of the rectangle
		/// </summary>
		public double width;

		/// <summary>
		/// height of the rectangle
		/// </summary>
		public double height;
	}
}
