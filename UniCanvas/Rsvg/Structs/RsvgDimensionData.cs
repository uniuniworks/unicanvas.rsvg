﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	/// <summary>
	/// Dimensions of an SVG image from rsvg_handle_get_dimensions(),
	/// or an individual element from rsvg_handle_get_dimensions_sub().
	/// Please see the deprecation documentation for those functions.
	/// </summary>
	//[Obsolete("Since version 2.46")]
	[StructLayout(LayoutKind.Sequential)]
	public class RsvgDimensionData {
		/// <summary>
		/// SVG's width, in pixels
		/// </summary>
		public int width;

		/// <summary>
		/// SVG's height, in pixels
		/// </summary>
		public int height;

		/// <summary>
		/// SVG's original width, unmodified by RsvgSizeFunc
		/// </summary>
		public double em;

		/// <summary>
		/// SVG's original height, unmodified by RsvgSizeFunc
		/// </summary>
		public double ex;
	}
}
