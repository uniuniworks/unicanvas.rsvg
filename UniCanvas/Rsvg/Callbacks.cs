﻿using System.Runtime.InteropServices;

namespace UniCanvas.Rsvg {
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void RsvgSizeFunc(
		ref int width,   // gint*
		ref int height,  // gint*
		byte[] user_data // gpointer
	);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void GDestroyNotify(
		byte[] data // gpointer
	);
}
