# UniCanvas.Rsvg

## Preparation

- Move all files in UniCanvas to Assets folder.

### Mac

Homebrew
```bash
$ brew install librsvg
```

## How to use

```cs
using UniCanvas;
using UniCanvas.Rsvg;

public class Test : MonoBehaviour {
	void Start() {
		var canvas = new OffscreenCanvas(256, 256);
		var ctx = canvas.getContext("2d");

		var svg = new Rsvg("Ghostscript_Tiger.svg");
		svg.RenderCairo(ctx.Context.NativePtr);
	}
}
```

## License

- GPL
